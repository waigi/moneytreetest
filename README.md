## Architecture of the Moneytree test project

The project is built with MVVM pattern.

1. Source codes are organized by modules with each group inside **Modules** group. Groups inside the project match the actual file folders.
2. The **Services** group contains all service clients by which calling backend APIs.
3. The **Account** group contains Account related stuff. Inside it, there are **View** group which contains custom views that are used on Account related screens, **ViewModel** group which has data model and view models for ViewController or Views, **Controller** group which holds account pages' view controllers.
4. For demostration purpose, two functions of the **AccountClient** are reading local JSON files (inside **Services** group as well) instead of making real network calls.
5. The **Common** group is the place to put App wide common stuffs such as extensions to the Swift/iOS APIs.
6. 

---

## Test cases

Test cases resides in MoneytreeTests group with exactly the same group structure of the project itself.

1. 
2. 
