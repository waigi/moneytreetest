//
//  AccountClientTests.swift
//  MoneytreeTests
//
//  Created by Can Zhan on 2/6/19.
//  Copyright © 2019 Can Zhan. All rights reserved.
//

import XCTest
@testable import Moneytree

class AccountClientTests: XCTestCase {

    func testFetchAccounts() {
        let expectation = XCTestExpectation(description: "service call")
        
        AccountClient.fetchAccounts { (result) in
            switch result {
            case .success(let accounts):
                XCTAssert(accounts.count == 3)
            case .failure(_):
                XCTFail()
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 60)
    }

    func testfetchTransactionsOf1() {
        let accountId = 1
        
        let expectation = XCTestExpectation(description: "service call")
        
        AccountClient.fetchTransactionsOf(accountId) { (result) in
            switch result {
            case .success(let transactions):
                XCTAssert(transactions.count == 1)
                let transaction = transactions[0]
                XCTAssert(transaction.id == 11)
                XCTAssert(transaction.amount == -6850.0)
            case .failure(_):
                XCTFail()
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 60)
    }
    
    func testfetchTransactionsOf2() {
        let accountId = 2
        
        let expectation = XCTestExpectation(description: "service call")
        
        AccountClient.fetchTransactionsOf(accountId) { (result) in
            switch result {
            case .success(let transactions):
                XCTAssert(transactions.count == 5)
                let transaction = transactions[2]
                XCTAssert(transaction.id == 23)
                XCTAssert(transaction.amount == -421.0)
            case .failure(_):
                XCTFail()
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 60)
    }
    
    func testfetchTransactionsOf3() {
        let accountId = 3
        
        let expectation = XCTestExpectation(description: "service call")
        
        AccountClient.fetchTransactionsOf(accountId) { (result) in
            switch result {
            case .success(let transactions):
                XCTAssert(transactions.isEmpty)
            case .failure(_):
                XCTFail()
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 60)
    }

}
