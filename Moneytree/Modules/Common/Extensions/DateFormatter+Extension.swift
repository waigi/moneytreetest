//
//  DateFormatter+Extension.swift
//  Moneytree
//
//  Created by Can Zhan on 2/6/19.
//  Copyright © 2019 Can Zhan. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    /// Date formatter to parse API datetime. e.g. "2017-05-26T00:00:00+09:00"
    static let apiDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXX"
        dateFormatter.calendar = Calendar(identifier: .iso8601)
        return dateFormatter
    }()
    
    /// Date decoding strategy to decode API datetime. e.g. "2017-05-26T00:00:00+09:00"
    static let apiDateDecodingStrategy: JSONDecoder.DateDecodingStrategy = {
        return .formatted(DateFormatter.apiDateFormatter)
    }()
    
    /// Date formatter to show transaction date
    static let displayDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.calendar = Calendar(identifier: .iso8601)
        return dateFormatter
    }()
    
    /// Date formatter to show transaction date
    static let transactionDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM yyyy"
        dateFormatter.calendar = Calendar(identifier: .iso8601)
        return dateFormatter
    }()
    
}
