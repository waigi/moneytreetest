//
//  Date+Extension.swift
//  Moneytree
//
//  Created by Can Zhan on 3/6/19.
//  Copyright © 2019 Can Zhan. All rights reserved.
//

import Foundation

extension Date {
    
    /// Calculate the first date from the given date (Ex:- Given date is 27/08/2018 then first date of the month is 01/08/2018)
    var startDateOfTheMonth: Date {
        let currentDateComponents = Calendar.current.dateComponents([.year, .month], from: self)
        return Calendar.current.date(from: currentDateComponents) ?? Date()
    }
    
}
