//
//  NumberFormatter.swift
//  Moneytree
//
//  Created by Can Zhan on 3/6/19.
//  Copyright © 2019 Can Zhan. All rights reserved.
//

import Foundation

extension NumberFormatter {
    
    static func currencyNumberFormatterOf(_ currency: String) -> NumberFormatter {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.groupingSize = 3
        switch currency.uppercased() {
        case "JPY":
            currencyFormatter.maximumFractionDigits = 0
            currencyFormatter.minimumFractionDigits = 0
        default:
            currencyFormatter.maximumFractionDigits = 2
            currencyFormatter.minimumFractionDigits = 2
        }
        return currencyFormatter
    }
    
}
