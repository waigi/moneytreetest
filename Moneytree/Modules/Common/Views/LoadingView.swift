//
//  LoadingView.swift
//  Moneytree
//
//  Created by Can Zhan on 2/6/19.
//  Copyright © 2019 Can Zhan. All rights reserved.
//

import UIKit

class LoadingView: UIView {
    
    private let indicator = UIActivityIndicatorView(style: .whiteLarge)
    
    // MARK: - Init
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.60)
        
        indicator.startAnimating()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(indicator)
        indicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        layoutIfNeeded()
    }
}

// MARK: - Hide & Show
extension LoadingView {
    
    /// Show the fullscreen loading view
    func show() {
        indicator.startAnimating()
        alpha = 0.0
        frame = UIScreen.main.bounds
        UIApplication.shared.keyWindow?.addSubview(self)
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.alpha = 1.0
        }
    }
    
    /// Hide the fullscreen loading view
    func hide() {
        indicator.stopAnimating()
        UIView.animate(
            withDuration: 0.3,
            animations: { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.alpha = 0.0
            },
            completion: { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.removeFromSuperview()
        })
    }
    
}

