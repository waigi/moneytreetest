//
//  RoundCornerView.swift
//  Moneytree
//
//  Created by Can Zhan on 3/6/19.
//  Copyright © 2019 Can Zhan. All rights reserved.
//

import UIKit

/// Provide a round corner view with configurable background color
@IBDesignable class RoundCornerView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 5.0
    @IBInspectable var borderWidth: CGFloat = 0.0
    @IBInspectable var borderColor: UIColor = .black
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        clipsToBounds = true
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
    }
    
}
