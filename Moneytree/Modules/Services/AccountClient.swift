//
//  AccountClient.swift
//  Moneytree
//
//  Created by Can Zhan on 2/6/19.
//  Copyright © 2019 Can Zhan. All rights reserved.
//

import Foundation

struct AccountClient {
    
    enum MoneytreeError: Error {
        case cannotFindResource
        case cannotDecode(Error)
    }
    
    static func fetchAccounts(completion: @escaping (Result<[Account], MoneytreeError>) -> Void) {
        guard let url = Bundle.main.url(forResource: "accounts.json", withExtension: nil),
            let data = try? Data.init(contentsOf: url)
            else {
                completion(.failure(.cannotFindResource))
                return
        }
        do {
            let accounts = try JSONDecoder().decode(AccountsResponse.self, from: data).accounts
            completion(.success(accounts))
        } catch {
            completion(.failure(.cannotDecode(error)))
        }
    }
    
    static func fetchTransactionsOf(_ accountId: Int, completion: @escaping (Result<[Transaction], MoneytreeError>) -> Void) {
        guard let url = Bundle.main.url(forResource: "transactions_\(accountId).json", withExtension: nil),
            let data = try? Data.init(contentsOf: url)
            else {
                completion(.failure(.cannotFindResource))
                return
        }
        do {
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = DateFormatter.apiDateDecodingStrategy
            let transactions = try decoder.decode(TransactionsResponse.self, from: data).transactions
            completion(.success(transactions))
        } catch {
            completion(.failure(.cannotDecode(error)))
        }
    }
}
