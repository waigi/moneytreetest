//
//  AccountCell.swift
//  Moneytree
//
//  Created by Can Zhan on 3/6/19.
//  Copyright © 2019 Can Zhan. All rights reserved.
//

import UIKit

class AccountCell: UITableViewCell {

    @IBOutlet weak var nicknameLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    
    var viewModel: AccountCellViewModel? {
        didSet {
            guard let viewModel = viewModel else { return }
            nicknameLabel.text = viewModel.account.nickname
            balanceLabel.text = viewModel.currencyBalanceString
        }
    }

}

struct AccountCellViewModel {
    
    let account: Account
    
    var currencyBalanceString: String {
        return "\(account.currency)\(NumberFormatter.currencyNumberFormatterOf(account.currency).string(from: NSNumber(value: account.currentBalance)) ?? "")"
    }
    
}
