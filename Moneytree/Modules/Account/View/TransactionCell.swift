//
//  TransactionCell.swift
//  Moneytree
//
//  Created by Can Zhan on 3/6/19.
//  Copyright © 2019 Can Zhan. All rights reserved.
//

import UIKit

class TransactionCell: UITableViewCell {
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!

    var viewModel: TransactionCellViewModel? {
        didSet {
            guard let viewModel = viewModel else { return }
            idLabel.text = String(viewModel.transaction.id)
            dateLabel.text = viewModel.dateString
            descriptionLabel.text = viewModel.transaction.description
            amountLabel.text = viewModel.currencyAmountString
        }
    }
}

struct TransactionCellViewModel {
    
    let account: Account
    let transaction: Transaction
    
    var dateString: String {
        return DateFormatter.displayDateFormatter.string(from: transaction.date)
    }
    
    var currencyAmountString: String {
        let isPositive = transaction.amount >= 0
        let absAmount = NSNumber(value: abs(transaction.amount))
        return "\(isPositive ? "" : "-")\(account.currency)\(NumberFormatter.currencyNumberFormatterOf(account.currency).string(from: absAmount) ?? "")"
    }
    
}
