//
//  AccountViewController.swift
//  Moneytree
//
//  Created by Can Zhan on 2/6/19.
//  Copyright © 2019 Can Zhan. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {

    /// Build an instance of WhatsNewViewController with view model
    static func instance(viewModel: AccountViewModel) -> AccountViewController? {
        guard let viewController = UIStoryboard(name: "Account", bundle: nil).instantiateViewController(withIdentifier: "AccountViewController") as? AccountViewController else { return nil }
        viewController.viewModel = viewModel
        return viewController
    }
    
    @IBOutlet weak var nicknameLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    private var loadingView = LoadingView()
    var viewModel: AccountViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadingView.show()
        viewModel.delegate = self
        viewModel.fetchTransactions()
    }
    
    private func setupUI() {
        navigationItem.title = viewModel.account.institution
        balanceLabel.text = nil
        tableView.estimatedRowHeight = 40.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
    }
    
    private func refreshUI() {
        nicknameLabel.text = viewModel.account.nickname
        balanceLabel.text = viewModel.balanceString
        tableView.reloadData()
    }

}

extension AccountViewController: AccountDelegate {
    func transactionsDidUpdate() {
        refreshUI()
        loadingView.hide()
    }
    
    func transactionsFailedUpdate() {
        loadingView.hide()
    }
}

extension AccountViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "transactionCell", for: indexPath)
        if let cell = cell as? TransactionCell {
            cell.viewModel = viewModel.cellViewModelAt(indexPath)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.sectionHeaderInSection(section)
    }
    
}

extension AccountViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let view = view as? UITableViewHeaderFooterView {
            view.contentView.backgroundColor = .red
            view.textLabel?.textColor = .white
        }
    }
}
