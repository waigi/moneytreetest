//
//  AccountsViewController.swift
//  Moneytree
//
//  Created by Can Zhan on 3/6/19.
//  Copyright © 2019 Can Zhan. All rights reserved.
//

import UIKit

class AccountsViewController: UIViewController {
    
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    private var loadingView = LoadingView()
    private var viewModel = AccountsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadingView.show()
        viewModel.delegate = self
        viewModel.fetchAccounts()
    }
    
    private func setupUI() {
        navigationItem.title = "Balances"
        tableView.estimatedRowHeight = 40.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
    }
    
    private func refreshUI() {
        balanceLabel.text = viewModel.balanceString
        tableView.reloadData()
    }

}

extension AccountsViewController: AccountsDelegate {
    func accountsDidUpdate() {
        refreshUI()
        loadingView.hide()
    }
    
    func accountsFailedUpdate() {
        loadingView.hide()
    }
}

extension AccountsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "accountCell", for: indexPath)
        if let cell = cell as? AccountCell {
            cell.viewModel = viewModel.cellViewModelAt(indexPath)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.sectionHeaderInSection(section)
    }
    
}

extension AccountsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let view = view as? UITableViewHeaderFooterView {
            view.contentView.backgroundColor = .red
            view.textLabel?.textColor = .white
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let account = viewModel.accountAt(indexPath)
        if let accountViewController = AccountViewController.instance(viewModel: AccountViewModel(account: account)) {
            navigationController?.pushViewController(accountViewController, animated: true)
        }
    }
}
