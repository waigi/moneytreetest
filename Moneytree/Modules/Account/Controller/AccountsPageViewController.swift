//
//  AccountsPageViewController.swift
//  Moneytree
//
//  Created by Can Zhan on 2/6/19.
//  Copyright © 2019 Can Zhan. All rights reserved.
//

import UIKit

class AccountsPageViewController: UIPageViewController {
    
    static func instance() -> AccountsPageViewController {
        let viewController = AccountsPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        return viewController
    }
    
    private var pageViewControllers = [UIViewController]()
    private var loadingView = LoadingView()
    private var pageControl: UIPageControl?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        loadAccountPages()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for view in view.subviews {
            if let view = view as? UIScrollView {
                view.frame = UIScreen.main.bounds
            } else if let view = view as? UIPageControl {
                pageControl = view
                if pageViewControllers.count > 1 {
                    // Show page indicator when there is more than one pages
                    view.pageIndicatorTintColor = .orange
                    view.backgroundColor = .clear
                    view.frame.origin.y += 16.0
                    self.view.bringSubviewToFront(view)
                }
            }
        }
    }
    
    private func loadAccountPages() {
        let accountViewController = UIStoryboard(name: "Account", bundle: nil).instantiateViewController(withIdentifier: "AccountsViewController")
        let navigationViewController = UINavigationController(rootViewController: accountViewController)
        pageViewControllers.append(navigationViewController)
    }

}

// MARK: - Serve pages
extension AccountsPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = pageViewControllers.firstIndex(of: viewController) else { return nil }
        return index > 0 ? pageViewControllers[index - 1] : nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = pageViewControllers.firstIndex(of: viewController) else { return nil }
        return index < pageViewControllers.count - 1 ? pageViewControllers[index + 1] : nil
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return pageViewControllers.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
}
