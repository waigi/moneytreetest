//
//  AccountViewModel.swift
//  Moneytree
//
//  Created by Can Zhan on 3/6/19.
//  Copyright © 2019 Can Zhan. All rights reserved.
//

import Foundation

protocol AccountDelegate: class {
    func transactionsDidUpdate()
    func transactionsFailedUpdate()
}

class AccountViewModel {
    
    let account: Account
    
    init(account: Account) {
        self.account = account
    }
    
    private var transactions = [Transaction]() {
        didSet {
            transactionsMap.removeAll()
            // Group the trasaction by the month and year
            transactionsMap = Dictionary(grouping: transactions, by: { $0.date.startDateOfTheMonth }).sorted(by: { $0.key > $1.key }).map { ($0.key, $0.value.sorted(by: { $0.date > $1.date }))}
            monthNames = transactionsMap.map{ $0.key }.map{ DateFormatter.transactionDateFormatter.string(from: $0) }
        }
    }
    
    /// Transactions grouped by month
    private var transactionsMap = [(key: Date, value: [Transaction])]()
    
    /// Sorted month names
    private var monthNames = [String]()
    
    var delegate: AccountDelegate?
    
    // MARK: - Table data
    
    var numberOfSections: Int {
        return monthNames.count
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return transactionsMap[section].value.count
    }
    
    func sectionHeaderInSection(_ section: Int) -> String {
        return monthNames[section]
    }
    
    func cellViewModelAt(_ indexPath: IndexPath) -> TransactionCellViewModel {
        return TransactionCellViewModel(account: account, transaction: transactionsMap[indexPath.section].value[indexPath.row])
    }
    
    var balanceString: String {
        return "\(account.currency.uppercased())\(NumberFormatter.currencyNumberFormatterOf(account.currency).string(from: NSNumber(value: account.currentBalance)) ?? "")"
    }
    
    // MARK: - Load data
    
    func fetchTransactions() {
        AccountClient.fetchTransactionsOf(
            account.id,
            completion: { [weak self] (result) in
                guard let strongSelf = self else { return }
                switch result {
                case .success(let transactions):
                    strongSelf.transactions = transactions
                    strongSelf.delegate?.transactionsDidUpdate()
                case .failure(_):
                    strongSelf.delegate?.transactionsFailedUpdate()
                }
        })
    }
    
}
