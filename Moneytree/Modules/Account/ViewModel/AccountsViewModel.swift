//
//  AccountsViewModel.swift
//  Moneytree
//
//  Created by Can Zhan on 3/6/19.
//  Copyright © 2019 Can Zhan. All rights reserved.
//

import Foundation

protocol AccountsDelegate: class {
    func accountsDidUpdate()
    func accountsFailedUpdate()
}

class AccountsViewModel {
    
    private var accounts = [Account]() {
        didSet {
            accountsMap.removeAll()
            accounts.forEach { account in
                if var accounts = accountsMap[account.institution] {
                    accounts.append(account)
                    accountsMap[account.institution] = accounts
                } else {
                    accountsMap[account.institution] = [account]
                }
            }
            institutionNames = accountsMap.keys.sorted()
        }
    }
    
    /// Accounts grouped by institution names
    private var accountsMap = [String: [Account]]()
    
    /// Sorted institution names
    private var institutionNames = [String]()
    
    var delegate: AccountsDelegate?
    
    var balanceString: String {
        let amount = accounts.reduce(0, { (result, account) -> Double in
            return result + account.currentBalanceInBase
        })
        return "JPY\(NumberFormatter.currencyNumberFormatterOf("JPY").string(from: NSNumber(value: amount)) ?? "")"
    }
    
    // MARK: - Table data
    
    var numberOfSections: Int {
        return institutionNames.count
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return accountsMap[institutionNames[section]]?.count ?? 0
    }
    
    func sectionHeaderInSection(_ section: Int) -> String {
        return institutionNames[section]
    }
    
    func cellViewModelAt(_ indexPath: IndexPath) -> AccountCellViewModel {
        return AccountCellViewModel(account: accountAt(indexPath))
    }
    
    func accountAt(_ indexPath: IndexPath) -> Account {
        let institutionName = institutionNames[indexPath.section]
        let accounts = accountsMap[institutionName]!
        return accounts[indexPath.row]
    }
    
    // MARK: - Load data
    
    func fetchAccounts() {
        AccountClient.fetchAccounts { [weak self] (result) in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let accounts):
                strongSelf.accounts = accounts
                strongSelf.delegate?.accountsDidUpdate()
            case .failure(_):
                strongSelf.delegate?.accountsFailedUpdate()
            }
        }
    }
    
}
