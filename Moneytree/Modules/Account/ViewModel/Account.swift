//
//  Account.swift
//  Moneytree
//
//  Created by Can Zhan on 2/6/19.
//  Copyright © 2019 Can Zhan. All rights reserved.
//

import Foundation

struct AccountsResponse: Decodable {
    let accounts: [Account]
    
    private enum CodingKeys: String, CodingKey {
        case accounts
    }
}

struct Account: Decodable {
    let id: Int
    let nickname: String
    let institution: String
    let currency: String
    let currentBalance: Double
    let currentBalanceInBase: Double
    
    private enum CodingKeys: String, CodingKey {
        case id, nickname, institution, currency
        case currentBalance = "current_balance"
        case currentBalanceInBase = "current_balance_in_base"
    }
}


/** Sample JSON
 {
 "accounts": [
 {
 "id": 1,
 "nickname": "外貨普通(USD)",
 "institution": "Test Bank",
 "currency": "USD",
 "current_balance": 22.5,
 "current_balance_in_base": 2306.0
 },
 {
 "id": 2,
 "nickname": "マークからカード",
 "institution": "Starbucks Card",
 "currency": "JPY",
 "current_balance": 3035.0,
 "current_balance_in_base": 3035.0
 
 },
 {
 "id": 3,
 "nickname": "マイカード",
 "institution": "Starbucks Card",
 "currency": "JPY",
 "current_balance": 0.0,
 "current_balance_in_base": 0.0
 }
 ]
 }
*/
